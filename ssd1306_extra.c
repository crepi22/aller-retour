
#include "ssd1306.h"
#include "ssd1306_extra.h"
extern const uint8_t font_8x5[];

void ssd1306_invert_horizontal_line(ssd1306_t *p, uint32_t y) {
    if(y>=p->height) return;
    for(int x=0; x < p->width; x++) {
        p->buffer[x + p->width*(y>>3)]^=0x1<<(y&0x07);
    }
}
