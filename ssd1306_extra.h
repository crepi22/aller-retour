/**
* @file ssd1306_extra.h
*
* extra string functions for ssd1306 displays
*/

#ifndef _inc_ssd1306_extra
#define _inc_ssd1306_extra
#include "ssd1306.h"

/**
	@brief invert an horizontal line of screen

	@param[in] p : instance of display
	@param[in] y : y position of line
*/
void ssd1306_invert_horizontal_line(ssd1306_t *p, uint32_t y);

#endif
