# Aller retour programmable.

Commande un pont en H pour un train analogique faisant un aller retour
sur un tronçon découpé en trois blocs. L'entrée dans les blocs
d'extrémité déclenche le ralentissement, l'arrêt, puis le redémarrage
en sens inverse. Les durées des différentes étapes et la vitesse
maximale sont programmables.

## Circuit

* Raspberry Pi Pico
* module H-Bridge TI DRV8871
* Ecran OLED monochrome 128x64 I2C piloté par un SSD1306
* Encodeur rotatif (type Keynes KY-040)
* Une alimentation stabilisée 5v pour le raspberry pico (ex module Mini 560)
* 2 détecteurs de présence constitués des composants suivants:
    * optocoupleur H11AA1 (c'est un optocoupleur bidirectionnel. Si vous utilisez un optocoupleur type 4N35 attention au sens du courant dans le circuit)
    * Pont de diode
    * Résistance 22ohm
    * Condensateur 10 µF

Note : on utilise le pull-up du pico à la sortie de l'optocoupleur. La constante de temps est
probablement un peu trop élevée (résistance de l'ordre de 50kOhm soit près de 0.5s). On peut aussi utiliser une résistance 10kOhm reliée au 3.3v pour un meilleur contrôle de la réactivité du circuit.

![Diagramme circuit aller/retour](images/aller-retour_bb.png)*Diagramme Fritzing*

La version finale utilise une carte PCB d'expérimentation 5x7cm. Les circuits sont fixés
sur des planches de medium et des cartons épais.

## Utilisation
A l'allumage le circuit est immédiatement opérationnel. Il faut appuyer sur le bouton
du rotoencodeur pour accéder au menu de réglage (le train s'arrête). Les paramètres sont
les suivants:
* vitesse de A à B (en % de la tension crête)
* vitesse de B à A (en % de la tension crête)
* Durée de décélération en A (en dixième de s)
* Durée d' accélération en A (en dixième de s)
* Durée d'arrêt en A (en s)
* Durée de décélération en B (en dixième de s)
* Durée d' accélération en B (en dixième de s)
* Durée d'arrêt en B (en s)
* Inversion du courant. Si au démarrage le train se dirrige vers
  A et non vers B, l'inversion de courant permet de corriger le problème.
* L'entrée .. permet de quitter le menu de configuration et lancer le module

Les paramètres sont stockés sur la flash et sont préservés entre chaque redémarrage.

## Licence
Distribué sous licence Apache.

Ce logiciel inclut la bibliothèque pour écran OLED de David Schramm
(https://github.com/daschr/pico-ssd1306 - licence MIT)

Module développé pour un circuit de l'AMPT (http://ampmodelisme.e-monsite.com/)
