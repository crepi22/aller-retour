/*
Copyright 2022 Pierre Crégut

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/i2c.h"
#include "hardware/gpio.h"
#include "hardware/pwm.h"
#include "hardware/flash.h"
#include "ssd1306.h"
#include "ssd1306_extra.h"

#define SLEEP_TIME 25
#define I2C_DATA 16
#define I2C_SCL 17

#define PWM1 6
#define PWM2 7

#define ROT_CLK 4
#define ROT_STA 3
#define ROT_SW 2
#define ENA_BU 5

#define SENSOR_A 8
#define SENSOR_B 9

#define SWITCH_BOUNCE_DELAY 1000

#define FLASH_TARGET_OFFSET (256 * 1024)

const uint8_t *flash_target_contents = (const uint8_t *) (XIP_BASE + FLASH_TARGET_OFFSET);

const uint8_t CLOCK_BIT = 4;
const uint8_t DATA_BIT = 2;
const uint8_t SWITCH_BIT = 1;

enum track_state {
    AtoB,
    DecelB,
    StopB,
    AccelB,
    BtoA,
    DecelA,
    StopA,
    AccelA,
    Menu
};

enum rot_encoder_state {WaitForClock, WaitForData};

enum rot_action {
    RotNone,
    RotInc,
    RotDec
};

struct parameter {
    uint8_t decA; // tenth of sec
    uint8_t stopA; // seconds
    uint8_t accA; // tenth of sec
    uint8_t decB; // tenth of sec
    uint8_t stopB; // seconds
    uint8_t accB; // tenth of sec
    uint8_t speedAB; // percent
    uint8_t speedBA; // percent
    uint8_t invert; // 0 or 1
};

struct parameter param = {
    20,
    3,
    40,
    8,
    6,
    40,
    50,
    30,
    0
};

enum menu_kind {
    Integer,
    Tenth,
    Submenu,
    Enumeration
};

struct menu_item {
    char *name;
    uint8_t size;
    enum menu_kind kind;
    struct menu_item *items;
    uint8_t *target;
};

struct menu_item main_menu_items[] = {
    {"decel A (s)", 99, Tenth, NULL, &param.decA},
    {"stop A (s)", 99, Integer, NULL, &param.stopA},
    {"accel A (s)", 99, Tenth, NULL, &param.accA},
    {"decel B (s)", 99, Tenth, NULL, &param.decB},
    {"stop B (s)", 99, Integer, NULL, &param.stopB},
    {"accel B (s)", 99, Tenth, NULL, &param.accB},
    {"Speed A->B (%)", 99, Integer, NULL, &param.speedAB},
    {"Speed B->A (%)", 99, Integer, NULL, &param.speedBA},
    {"Invert (0-1)", 2, Integer, NULL, &param.invert}
};

struct menu_item main_menu = {
    "Main", 10, Menu, main_menu_items
};

inline void debug(char *s){
    puts(s);
}

inline static void set_speed(uint8_t slice, uint16_t level_a, uint16_t level_b) {
    if (param.invert) { pwm_set_both_levels(slice, level_b, level_a); }
    else { pwm_set_both_levels(slice, level_a, level_b); }
}

void print_buf(const uint8_t *buf, size_t len) {
    for (size_t i = 0; i < len; ++i) {
        printf("%02x", buf[i]);
        if (i % 16 == 15)
            printf("\n");
        else
            printf(" ");
    }
}

void flash_program() {
    uint8_t flash_buffer[FLASH_PAGE_SIZE];
    memset(flash_buffer, 0, FLASH_PAGE_SIZE);
    memcpy(flash_buffer, &param, sizeof(struct parameter));
    debug("erasing flash");
    flash_range_erase(FLASH_TARGET_OFFSET, FLASH_SECTOR_SIZE);
    sleep_ms(100);
    debug("programming flash");
    flash_range_program(FLASH_TARGET_OFFSET, flash_buffer, FLASH_PAGE_SIZE);
    sleep_ms(100);
    if (memcmp(&param, flash_target_contents, sizeof(struct parameter)) != 0){
        debug("expected");
        print_buf(flash_buffer, FLASH_PAGE_SIZE);
        debug("Flash failed !!!!");
    }
}

void flash_init() {
    debug("flash init");
    if (flash_target_contents[16] == 0xFF){
        debug("Force program");
        flash_program();
    } else {
        debug("load");
        memcpy(&param, flash_target_contents, sizeof(struct parameter));
    }
}

void flash_save() {
    debug("check save needed");
    if (memcmp(&param, flash_target_contents, sizeof(struct parameter)) != 0){
        debug("- save");
        flash_program();
    } else {
        debug("- nope");
    }
}

void screensaver(ssd1306_t *disp){
    ssd1306_clear(disp);
    ssd1306_draw_string(disp, 40, 8, 6, "*");
    ssd1306_show(disp);
}

void display(ssd1306_t *disp, uint8_t out, struct menu_item *item) {
    char buffer[8];
    ssd1306_clear(disp);
    printf("out=%d\n", out);
    switch(item->kind){
        case Integer:
            debug("disp integ");
            ssd1306_draw_string(disp, 8, 4, 1, item->name);
            sprintf(buffer, "%2d", out);
            ssd1306_draw_string(disp, 40, 16, 5, buffer);
            break;
        case Tenth:
            debug("disp tenth");
            ssd1306_draw_string(disp, 8, 4, 1, item->name);
            sprintf(buffer, "%d.%d", out/10, out%10);
            ssd1306_draw_string(disp, 20, 16, 5, buffer);
            break;
        case Menu:
            debug("disp menu");
            for(int i=0; i < 5; i++){
                int p = (out + i + item->size - 2) % item->size;
                char *s = (p==0) ? ".." : item->items[p-1].name;
                ssd1306_draw_string(disp, 8, i*10, 1, s);
            }
            for(int y=20; y<30; y++) {
                ssd1306_invert_horizontal_line(disp, y);
            }
            break;
    }
    ssd1306_show(disp);
    debug("end disp");
}

#define RATIO 500

uint setup_pwm(void) {
    gpio_set_function(PWM1, GPIO_FUNC_PWM);
    gpio_set_function(PWM2, GPIO_FUNC_PWM);
    uint slice = pwm_gpio_to_slice_num(PWM1);
    pwm_set_clkdiv(slice, 8.0f);
    pwm_set_wrap(slice, 99 * RATIO);
    set_speed(slice, 0, 0);
    pwm_set_enabled(slice, true);
    return slice;
}

void setup_sensors(void) {
    uint32_t mask = (1 << SENSOR_A) | (1 << SENSOR_B);
    gpio_init(mask);
    gpio_set_dir_in_masked(mask);
    gpio_pull_up(SENSOR_A);
    gpio_pull_up(SENSOR_B);
}

void setup_i2c(void) {
    i2c_init(i2c0, 400000);
    gpio_set_function(I2C_DATA, GPIO_FUNC_I2C);
    gpio_set_function(I2C_SCL, GPIO_FUNC_I2C);
    gpio_pull_up(I2C_DATA);
    gpio_pull_up(I2C_SCL);
}

void setup_switch(void){
    uint32_t mask = (1 << ROT_CLK) | (1 << ROT_STA) | (1 << ROT_SW);
    gpio_init(mask);
    gpio_set_dir_in_masked(mask);
    gpio_init(ENA_BU);
    gpio_set_dir_out_masked(1 << ENA_BU);
    gpio_set_mask(1 << ENA_BU);
}

void setup_display(ssd1306_t *disp){
    disp->external_vcc=false;
    ssd1306_init(disp, 128, 64, 0x3C, i2c0);
}

uint16_t switch_bounce = 0;

void run(uint pwm_slice) {
    enum track_state state = AtoB;
    uint32_t enterTime = 0;
    uint16_t bound;
    uint32_t delay;
    // We go toward B full speed
    debug("Running");
    set_speed(pwm_slice, RATIO * param.speedAB, 0);
    for(;;) {
        uint32_t gpios = gpio_get_all();
        if (switch_bounce) {
            switch_bounce--;
        } else {
            if ((gpios & (1 << ROT_SW)) == 0) {
                // Button pushed we leave run to enter menu
                debug("Leaving");
                switch_bounce = SWITCH_BOUNCE_DELAY;
                set_speed(pwm_slice, 0, 0);
                return;
            }
        }
        switch(state) {
            case AtoB:
                if((gpios & (1 << SENSOR_B)) == 0) {
                    debug("DecelB");
                    state = DecelB;
                    enterTime = time_us_32();
                }
                break;
            case DecelB:
                // Compute millis since enter DecelB
                delay = (time_us_32() - enterTime) / 1000;
                uint16_t bound = param.decB * 100;
                if (delay > bound) {
                    debug("StopB");
                    state = StopB;
                    enterTime = time_us_32();
                    set_speed(pwm_slice, 0, 0);
                } else {
                    uint32_t speed = RATIO * param.speedAB * (bound - delay) / bound;
                    set_speed(pwm_slice, speed, 0);
                }
                break;
            case StopB:
                delay = (time_us_32() - enterTime);
                if (delay > param.stopB * 1000000) {
                    enterTime = time_us_32();
                    debug("AccelB");
                    state = AccelB;
                }
                break;
            case AccelB:
                delay = (time_us_32() - enterTime) / 1000;
                bound = param.accB * 100;
                if (delay > bound) {
                    debug("BtoA");
                    state = BtoA;
                    set_speed(pwm_slice, 0, RATIO * param.speedBA);
                } else {
                    uint32_t speed = RATIO * param.speedBA * delay / bound;
                    set_speed(pwm_slice, 0, speed);
                }
                break;
            case BtoA:
                if((gpios & (1 << SENSOR_A)) == 0) {
                    debug("DecelA");
                    state = DecelA;
                    enterTime = time_us_32();
                }
                break;
            case DecelA:
                delay = (time_us_32() - enterTime) / 1000;
                bound = param.decA * 100;
                if (delay > bound) {
                    debug("StopA");
                    state = StopA;
                    enterTime = time_us_32();
                    set_speed(pwm_slice, 0, 0);
                } else {
                    uint32_t speed = RATIO * param.speedBA * (bound - delay) / bound;
                    set_speed(pwm_slice, 0, speed);
                }
                break;
            case StopA:
                delay = (time_us_32() - enterTime);
                if (delay > param.stopA * 1000000) {
                    enterTime = time_us_32();
                    debug("AccelA");
                    state = AccelA;
                }
                break;
            case AccelA:
                delay = (time_us_32() - enterTime) / 1000;
                bound = param.accA * 100;
                if (delay > bound) {
                    debug("AtoB");
                    state = AtoB;
                    set_speed(pwm_slice, RATIO * param.speedAB, 0);
                } else {
                    uint32_t speed = RATIO * param.speedAB * delay / bound;
                    set_speed(pwm_slice, speed, 0);
                }
                break;
        }
        sleep_ms(1);
    }
}

void animation(ssd1306_t *disp, struct menu_item *item) {
    uint8_t out = item->kind==Menu ? 0 : *item->target;
    uint32_t saved_gpios = 1 << ROT_CLK;
    uint8_t state = WaitForClock;
    debug("animation\n");
    display(disp, out, item);
    for(;;) {
        uint32_t gpios = gpio_get_all();
        enum rot_action action = RotNone;
        bool rot_switch = false;
        if (switch_bounce) {
            switch_bounce--;
        } else {
            if ((gpios & (1 << ROT_SW)) == 0) {
                // Button pushed we leave run to enter menu
                switch_bounce = SWITCH_BOUNCE_DELAY;
                rot_switch = true;
            }
        }
        switch (state) {
            case WaitForClock:
                if ((gpios ^ saved_gpios) & (1 << ROT_CLK)) {
                    if (gpios & (1 << ROT_CLK)) {
                        debug("Rotation");
                        if (gpios & (1 << ROT_STA)) {
                            action = RotDec;
                        } else {
                            action = RotInc;
                        }
                    }
                    saved_gpios = gpios;
                    state = WaitForData;
                }
                break;
            case WaitForData:
                if ((gpios ^ saved_gpios) & (1 << ROT_STA)) {
                    debug("Sta");
                    saved_gpios = gpios;
                    state = WaitForClock;
                }
                break;
        }
        if(rot_switch){
            debug("React to selection");
            if(item->kind == Menu) {
                if(out==0){
                    debug("Leave");
                    return;
                } else {
                    debug("Enter submenu");
                    animation(disp, &item->items[out-1]);
                    display(disp, out, item);
                }
            } else {
                // TODO: Set value
                *item->target = out;
                return;
            }
        } else {
             switch(action){
                case RotNone:
                    break;
                case RotInc:
                    out = (out + 1) % item->size;
                    display(disp, out, item);
                    break;
                case RotDec:
                    out = (out + item->size - 1) % item->size;
                    display(disp, out, item);
                    break;
            }
        }
        sleep_ms(1);
    }
}

int main() {
    // Delay so that the screen settles down before initialization
    sleep_ms(1000);
    stdio_init_all();
    setup_i2c();
    setup_switch();
    setup_sensors();
    uint pwm_slice = setup_pwm();
    ssd1306_t disp;
    setup_display(&disp);
    flash_init();
    for(;;) {
        screensaver(&disp);
        run(pwm_slice);
        animation(&disp, &main_menu);
        flash_save();
    }
}