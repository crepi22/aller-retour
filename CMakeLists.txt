cmake_minimum_required(VERSION 3.13)

include(pico_sdk_import.cmake)

project(pendule)

# initialize the Raspberry Pi Pico SDK
pico_sdk_init()

add_executable(pendule)

target_sources(pendule PRIVATE ssd1306.c ssd1306_extra.c main.c)

target_link_libraries(pendule PRIVATE pico_stdlib hardware_timer hardware_gpio hardware_i2c hardware_pwm hardware_flash)

pico_enable_stdio_usb(pendule 0)
pico_enable_stdio_uart(pendule 1)
pico_add_extra_outputs(pendule)
